#- You may have to change curveData to something else
#- everywhere it appears in this file

#< envPaths
require stream
require lakeshore240usb


epicsEnvSet("TOP",      "$(E3_CMD_TOP)")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(lakeshore240usb_DIR)/db/")
iocshLoad("$(lakeshore240usb_DIR)/lakeshore240.iocsh", "P=TS2-010CRM:,R=Cryo-TI-001:")

iocInit()

