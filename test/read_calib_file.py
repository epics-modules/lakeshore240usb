import re

file_name = "CX_LS_X132163.340"
with open(file_name) as f:
    l = f.readline()
    model = (l.split(" ")[-1]).split("\n")[0]
    l = f.readline()
    serial = (l.split(" ")[-1]).split("\n")[0]
    l = f.readline()
    dataformat = int(re.findall("[0-9]+", l)[0])
    l = f.readline()
    limit = float(re.findall("[0-9.]+", l)[0])
    l = f.readline()
    coef = int(re.findall("[0-9]+", l)[0])
    l = f.readline()
    size = float(re.findall("[0-9]+", l)[0])

    while len(l.split("No.")) <= 1:
    	l = f.readline()

    #empty line
    l = f.readline()

    units = []
    temps = []
    while(l != ''):
        l = f.readline()
        if (l != ''):
          n, u, t = re.findall("[0-9.]+", l)
          units.append(float(u))
          temps.append(float(t))

    print(units)
    print(temps)
