from epics import caget, caput
from time import sleep


P="lake:"

# select curve
caput(P + "#CurveNr", 1)

units = []
temps = []
for i in range(1,201):
    caput(P + "#PointPos", i)
    caput(P + "#Input", "1")
    sleep(0.1)

    if caget(P + "Unit-RB") == 0:
        continue

    units.append(caget(P + "Unit-RB"))
    temps.append(caget(P + "Temp-RB"))

caput(P + "Units-RB", units)
caput(P + "Temps-RB", temps)

print(caget(P + "Units-RB"))
print(caget(P + "Temps-RB"))
