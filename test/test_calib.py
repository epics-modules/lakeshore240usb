from time import sleep

from epics import caget, caput

P="lake:"

unit = [1.83001, 1.83787, 1.83788, 1.84262, 1.85534, 1.86704, 1.86903, 1.88857, 1.91982, 1.95188]
value = [300.565, 294.224, 294.216, 290.464, 280.654, 272.007, 270.542, 256.799, 236.387, 217.235]

caput(P + "curve", 1)
caput(P + "curveName", "curve1")
caput(P + "curveSerial", "CX_LS_X132163")
caput(P + "curveFMT", "4")
caput(P + "limitVal", "300.565")
caput(P + "curveCoeff", "1")

caput(P + "unit", unit)
caput(P + "value", value)

sleep(1)

caput(P + "uploadCurve.PROC", 1)
