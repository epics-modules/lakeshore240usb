# Kernel module to communicate through USB

This folder contains the files to build the cp210x module that is needed to 
communicate with Lakeshore240 USB port. This files were downloaded from:
https://www.silabs.com/documents/login/software/Linux_3.x.x_4.x.x_VCP_Driver_Source.zip

The cp210x was modified to include the lakeshore240 and to ignore one 
non-existent member of the gpio_chip struct:

```
57a58
>       { USB_DEVICE(0x1FB9, 0x0205) }, /* Lakeshore Model-240 */
2332c2333
<       priv->gc.parent = &serial->interface->dev;
---
>       //priv->gc.parent = &serial->interface->dev;
```

This kernel module should be converted to a dkms at some point.

## How to compile and enable the module on centos 7

* Install kernel-headers and kernel-devel packages
* reboot
* make (on the current folder)
* cp cp210x.ko to /lib/modules/<kernel-version>/kernel/drivers/usb/serial
* insmod cp210x.ko
* sudo chmod 666 /dev/ttyUSB0

This procedure doesn't make module be autoloaded on boot, after a reboot 
this should be executed:

* rmmod cp210x.ko
* insmod cp210x.ko
* sudo chmod 666 /dev/ttyUSB0
