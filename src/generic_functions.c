#include <stddef.h>
#include <stdlib.h>
#include <aSubRecord.h>


// Copy a waveform
static long waveform_comp(aSubRecord *psr) {
    //Load input waveform
    double * in1     = (double*)psr->a;
    long const in1_len = *((long*)psr->b);
    double * in2     = (double*)psr->c;
    long const in2_len = *((long*)psr->d);
    double const delta     = *((double*)psr->e);
    short i;


    if (in1_len != in2_len) {
        *((short*)psr->vala) = 0;
        return 0;
    }

    for(i=0;i<in1_len;i++){
        if (abs(in1[i]-in2[i]) > delta){
            *((short*)psr->vala) = 0;
            return 0;
        }
    }

    //success
    *((short*)psr->vala) = 1;
    return 0;
}


// Register the functions for access by EPICS
#include <registryFunction.h>
#include <epicsExport.h>

epicsRegisterFunction(waveform_comp);
